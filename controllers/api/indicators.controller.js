/** indicators.controller.js
 * This file have all methods to handler external requests about indicators
 */

var express = require('express');
var indicatorsService = require('../../services/indicatorsSendHistoric.service');

module.exports = {
   
  getTop20Indicators:function (req, res, next) {
      indicatorsService.getTop20Indicators(function(indicators){

            if (indicators) {
                res.json(indicators);
            } else {
                res.sendStatus(404);
            }
         
        });
    
    }
}