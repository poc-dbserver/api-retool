var constants = require('../../constants/notification.constants.json');
var devicesService = require('../../services/devices.service');
var sendFunction = require ('../../services/notification.service');

   module.exports = {
        sendNotification:function (image,indicator) {
               var credentials = {
            IonicApplicationID : "5660dc65",
            IonicApplicationAPItoken : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiIwNmE0NjE2OS04OTRhLTQxNGUtODM1Ny1mMzZiNTg0MDA0MmMifQ.KmkjwtquS6DoJTrBayiJ7sGoNkRB3eFub5thk31_Wqw"
            };
             devicesService.getDevices(function (devices) {
                if (devices) {
                    for (var device of devices ){
                        console.log(device.registrationId);
                       var notification = {
                                "tokens": [device.registrationId],
                                "profile": "test",
                                "notification": {

                                "android": {
                                "data":{
                                 "style": "inbox",
                                "summaryText": "There are %n% notifications",
                                "image": image,
                                "title": indicator.titulo,
                                "message": indicator.corpo
                                },
                               
                            },
                            "ios": {
                            "title": indicator.titulo,
                            "message": indicator.corpo
                            } 
                            }
                        };
                        sendFunction.sendMessage(credentials,notification, function(result){
                            
                            
                        });
                        
                        
                    } 
                } else {
                    console.log("erro");
                }
            })
        }
   }
   