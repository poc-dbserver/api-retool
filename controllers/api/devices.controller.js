/** devices.controller.js
 * This file have all methods to handler external requests about devices
 */
 
//Dependencies
var express = require('express');
var devicesService = require('../../services/devices.service');
var constants = require('../../constants/device.constants.json')


module.exports = {
    
    getDevices:function (req, res, next) {
        devicesService.getDevices(function (device) {
            if (device) {
                res.json(device);
            } else {
                res.sendStatus(404);
            }
        })
    },
    
    deleteDevices:function(req, res,next){
        devicesService.deleteDevices(req.params.device, function(result){
            if(result){
                res.send(constants.success.msg_del_success); 
            }else {
                res.status(400); 
            }
        })       
    },
    
    storeDevices:function(req,res,next){
        var deviceId = req.body.deviceId;
        var registrationId = req.body.registrationId;
        if ( typeof deviceId  == 'undefined' ||  typeof registrationId  == 'undefined' ) {
            console.log(constants.error.msg_invalid_param.message);
            res.json(constants.error.msg_invalid_param);
        } else if (!deviceId.trim() || !registrationId.trim() ) {
            console.log(constants.error.msg_empty_param.message);
            res.json(constants.error.msg_empty_param);
        } else {
            devicesService.storeDevices(deviceId, registrationId, function(result) {
                res.json(result);
            });
        }
    }
}
