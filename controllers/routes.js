//controllers/routes.js
/**
 *Mapping routes to send a request to controllers 
 * 
 */
var express = require('express');
var deviceController = require('./api/devices.controller');
var indicatorController = require('./api/indicators.controller');
var router = express.Router();


//Devices
router.post('/devices', deviceController.storeDevices); 
router.get('/devices',deviceController.getDevices);
router.delete('/devices/:device',deviceController.deleteDevices);
//Indicators
//router.get('/indicators/last',indicatorController.getLastIndicators);
router.get('/indicators/',indicatorController.getTop20Indicators);

module.exports = router;