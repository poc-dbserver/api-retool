//indicatorSendHistoric.service.js
/**
 * 
 *Here is the file with all methods to access, modify and delete indicator's send historic(s) from database 
 *
 */
 //Variables declaration
var constants = require('../constants/indicator.constants.historic.json');
var Indicator = require("../models/indicatorSendHistoric");
var notificationController = require("../controllers/api/notifications.controller");
//Service's array to export
var service = {};
service.insertIndicator = insertIndicatorHistoric;
service.getTop20Indicators = getTop20Indicators;
service.middlewareNotification = middlewareNotification;
service.getListIndicatorsHistoric = getListIndicatorsHistoric;
module.exports = service;

function getTop20Indicators (callback){      
        Indicator.find().limit(20).sort({$natural:-1}).exec( function (err, IndicatorMongo) {
        if (err) {
            callback(err);
        }
        else {
            callback(IndicatorMongo);
        }
    });
}
function getListIndicatorsHistoric (callback){
     Indicator.find({}).sort({$natural:-1}).exec( function (err, IndicatorMongo) {
        if (err) {
            callback(err);
        }
        else {
            callback(IndicatorMongo);
        }
     })
}


function insertIndicatorHistoric(indicator, callback){
    var newIndicator = new Indicator(indicator);
    newIndicator.save(function(err){
      
        if (!err){
            callback(newIndicator);

        } else {
            callback(constants.error.msg_reg_failure);
        };
    });
}
function middlewareNotification (indicator, callback) {
   if (indicator == "undefined"){
       callback("Indicador vazio");
   }
   Indicator.find({dataComparacao: indicator.dataComparacao, descricaoGrupo: indicator.descricaoGrupo}).sort({$natural:-1}).exec( function (IndicatorMongo) {

        if(IndicatorMongo == null) {
            if(indicator.tendencia == 1){
                var image = "https://cdn3.iconfinder.com/data/icons/musthave/256/Stock%20Index%20Up.png";
            }else {
                var image = "https://cdn3.iconfinder.com/data/icons/musthave/256/Stock%20Index%20Down.png";
            }
            notificationController.sendNotification(image, indicator);
            
        }
        
     })
}


