/**
 * 
 *Here is the file with the notification services methods  
 *
 */
 
//Variables declaration 
var constants = require('../constants/notification.constants.json');
var https = require('https');
var querystring = require('querystring');
/**
*This Method it's responsible to send a notification to device, using informations like RegistrationID, DeviceID, etc.
* @param credentials {json{string}} here we could find all relevant informations to send a notification
* @param notification {json{string}} Message and the atributtes relevants to send a notification
* @return a constant with a error or successful message

*/
exports.sendMessage = function(credentials,notification, callback){
 var postData = querystring.stringify(notification);
  var options = {
	hostname: 'api.ionic.io',
	path: '/push/notifications',
	method: 'POST',
	headers: {
	  "Content-Type" : "application/json",
	  "Authorization": "Bearer " + credentials.IonicApplicationAPItoken
	}
  };
    //console.log("ionic token"+credentials.IonicApplicationAPItoken);
  var req = https.request(options, function(res) {
   //console.log('STATUS: ' + res.statusCode);
    //console.log('HEADERS: ' + JSON.stringify(res.headers));
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
      //console.log('BODY: ' + chunk);
    });
  });

  req.on('error', function(e) {
    callback(constants.error.msg_send_failure);
  });

  req.write(JSON.stringify(notification));
  req.end();
  callback(constants.success.msg_send_success);

 
}

