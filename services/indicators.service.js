//indicator.service.js
/**
 * 
 *Here is the file with all methods to access, modify and delete indicators(s) from database 
 *
 */
 //Variables declaration
var constants = require('../constants/indicator.constants.json');
var Indicator = require("../models/indicator");

//Service's array to export
var service = {};
service.getLastIndicators = getLastIndicators;
service.feedIndicators = feedIndicators;
service.getTop20Indicators = getTop20Indicators;
module.exports = service;

/**
 * A function to retrieve the last indicator from DataBase 
 * @param callback - The callback that handles the response.
 * @param registrationId{string} - Id provide by the GCM API with Ionic API and used to find the device to delete
 * @return a error or a array with one indicator on it
 */
function getLastIndicators (callback){      
        Indicator.find().limit(1).sort({$natural:-1}).exec( function (err, IndicatorMongo) {
        if (err) {
            callback(err);
        }
        else {
            callback(IndicatorMongo[0]);
        }
    });
}

/**
 * A function to retrieve the last 20 indicators from DataBase 
 * @param callback - The callback that handles the response.
 * @return a error or a indicator's array
 */
function getTop20Indicators (callback){      
        Indicator.find().limit(20).sort({$natural:-1}).exec( function (err, IndicatorMongo) {
        if (err) {
            callback(err);
        }
        else {
            callback(IndicatorMongo);
        }
    });
}
/**
 * A function to feed with indicators the Mongo's DataBase. To do this, first all we call the getLastIndicator method to modify and create a new entrie.
 * @param callback - The callback that handles the response.
 * @return a error or a indicator save in database
 */
// function feedIndicators(callback){
//      getLastIndicators(function(lastIndicator){

//         var newIndicator = new Indicator();
//         newIndicator.data = Date.now();
//         newIndicator.dia =  newIndicator.data.getDate();
//         newIndicator.tendencia = lastIndicator.tendencia * (-1);
//         newIndicator.tipoGrupo = lastIndicator.tipoGrupo;
//         newIndicator.descricaoGrupo = lastIndicator.tipoGrupo;
//         newIndicator.indicador = lastIndicator.indicador;
//         newIndicator.tipoDadosComparacao = lastIndicator.tipoDadosComparacao;
//         newIndicator.titulo = lastIndicator.titulo;

//         if(newIndicator.tendencia == 1){
//             newIndicator.valorComparacao = lastIndicator.valor;
//             newIndicator.valor = lastIndicator.valor * 1,05;
//             newIndicator.corpo = "A loja Retool Praia de belas superou a média de vendas das últimas 4 semanas.";
//         }else {
//             newIndicator.valor = lastIndicator.valorComparacao * 0.95;
          
//             newIndicator.corpo = "A loja Retool Praia de belas apresentou queda na média de vendas em comparação com as últimas 4 semanas.";
//         }
         
         
//         insertIndicator(newIndicator,function(response){

//                 callback(newIndicator);
           
//       })
//     });
// }
function feedIndicators(callback){
       var indicatorGroup = [
            {
            tipoGrupo:"loja",
            descricaoGrupo : "loja Praia de Belas",
            titulo : "Retool Praia de Belas Media Vendas",
            corpo : "A loja Retool Praia de belas acabou de superar a média de vendas das últimas 4 semanas."
            },
            {
             tipoGrupo : "loja",
             descricaoGrupo : "loja Cachorro Quente do Rosário",
             titulo : "Retool Cachorro Quente do Rosário Media Vendas",
             corpo : "A loja Retool Cachorro Quente do Rosário acabou de superar a média de vendas das últimas 4 semanas."
            },
            {
             tipoGrupo : "categoria",
             descricaoGrupo : "categoria calças",
             titulo : "Retool Calças Media Vendas",
             corpo : "A loja Retool Praia de belas superou a média de vendas da categoria de calças das últimas 4 semanas."
            },
            {
            tipoGrupo : "categoria",
            descricaoGrupo : "categoria bebidas",
            titulo : "Retool Bebidas Media Vendas",
            corpo : "A loja Retool Cachorro Quente do Rosário superou a média de vendas da categoria de bebidas das últimas 4 semanas."
            },
            
            {
             tipoGrupo : "produto",
             descricaoGrupo : "produto Calça Levi's",
             titulo : "Retool Calça Levi's Media Vendas",
             corpo : "A loja Retool Praia de belas superou a média de vendas de calças da marca Levi's das últimas 4 semanas."
            },
            {
             tipoGrupo : "produto",
             descricaoGrupo : "produto Refrigerante Coca Cola",
             titulo : "Retool Refrigerante Coca Cola Media Vendas",
             corpo : "A loja Retool Cachorro Quente do Rosário superou a média de vendas do refrigerante Coca Cola das últimas 4 semanas."
            }
        ]
       
       var newIndicator = new Indicator();
       var randGroup = Math.floor((Math.random()*6)+1)-1;
       
       var groupSelected = indicatorGroup[randGroup];
       var newIndicator = new Indicator ();
       newIndicator.data = Date.now();
       newIndicator.dia =  newIndicator.data.getDate();
       var  dateComparacao = newIndicator.data;
       newIndicator.dataComparacao = dateComparacao.getDate()+"-"+dateComparacao.getMonth()+"-"+dateComparacao.getFullYear();

       newIndicator.tendencia = 1;
       newIndicator.indicador = "media_4_ultimas_semanas";
       newIndicator.tipoDadosComparacao = "vendas";
       newIndicator.titulo = groupSelected.titulo;
       newIndicator.corpo = groupSelected.corpo;
       newIndicator.tipoGrupo = groupSelected.tipoGrupo;
       newIndicator.descricaoGrupo = groupSelected.descricaoGrupo;
       newIndicator.valor = 3000000;
       newIndicator.valorComparacao = 2500000;
       insertIndicator(newIndicator,function(response){
           if (response == constants.error.msg_reg_failure){

               callback( response);
           }else if(response != constants.error.msg_reg_failure){
           callback(newIndicator)
           
           }
           })
       
}
/**
 * A function to feed with indicators the Mongo's DataBase. To do this, first all we call the getLastIndicator method to modify and create a new entrie.
 * @param callback - The callback that handles the response.
 * @param indicator{Indicator} - Object to be save in database
 * @return a constant with a error or successful message
 */
function insertIndicator(indicator, callback){
    var newIndicator = new Indicator(indicator);
    newIndicator.save(function(err){
      
        if (!err){
            console.log(constants.success.msg_reg_success);
            callback(constants.success.msg_reg_success);

        } else {
            callback(constants.error.msg_reg_failure);
        };
    });
}