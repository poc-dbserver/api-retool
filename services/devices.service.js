//devices.service.js
/**
 * 
 *Here is the file with all methods to access, modify and delete device(s) from database 
 *
 */
 
//Variables declaration
var constants = require('../constants/device.constants.json');
//Devices's model schema
var Device = require('../models/device');
var request = require('request');
var service = {};

//Array with the methods to export
service.storeDevices = storeDevices;
service.getDevices = getDevices;
service.deleteDevices = deleteDevices;
module.exports = service;

/**
 *A function to retrieve all devices from DataBase 
 * @param callback - The callback that handles the response.
 * @return an array with devices on it or a constant with a error
 */
function getDevices (callback){
       
       Device.find({}, {
        _id: false,
        __v: false
    }, function(err, devices) {
        if (!err) {
            callback(devices)   
        }else {
            callback(constants.error.msg_find_failure+": "+err);
        }
        
        
    });
}
/**
 * A function to store a device in DataBase 
 * @param callback - The callback that handles the response.
 * @param deviceID{string} - Id provide by the user's device
 * @param registrationId{string} - Id provide by the GCM API with Ionic API
 * @return a constant with a error or successful message
 */
 
 
function storeDevices (deviceId,registrationId,callback){
 
    var newDevice = new Device({ 
 
        deviceId : deviceId,
        registrationId : registrationId
    });
 
    Device.findOne({deviceId:deviceId}, function(err,device){
        console.log(device);
        if (device != null && device.registrationId == registrationId){
            console.log("Device ja existe");
             callback(constants.error.msg_reg_exists);
        }else if (device != null){
            Device.remove(device,function(err) {
                if (err) throw err;
                console.log("Device removido");
                newDevice.save(function(err){
                
                    if (!err) {
                        console.log("Salvou");
                        callback(constants.success.msg_reg_success);
     
                    } else {
                        callback(constants.error.msg_reg_failure);
     
                    }
                });
            });
        } else {
            newDevice.save(function(err){
                console.log("Simplesmente salvou");
                if (!err) {
                    callback(constants.success.msg_reg_success);
 
                } else {
                    callback(constants.error.msg_reg_failure);
 
                }
            });
        } 
 
    });
}
/**
 * A function to delete a device in DataBase 
 * @param callback - The callback that handles the response.
 * @param registrationId{string} - Id provide by the GCM API with Ionic API and used to find the device to delete
 * @return a constant with a error or successful message
 */
function deleteDevices(registrationId,callback){
    console.log("registrationId"+registrationId);
    Device.findOneAndRemove({registrationId:registrationId},function(err, data){
        if (!err) {
 
            
            if(data == null){  callback(constants.error.msg_del_failure);
            }else {
                  callback(constants.success.msg_del_success);
            }
 
        } else {
            callback(constants.error.msg_del_failure);
          
        }
    });
}
