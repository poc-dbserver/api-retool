/** app.js
*This file have all dependencies, a timer to fill the database with indicators, another time to send notifications and
*initialize the server
*/
//Dependencies
var express = require('express');
var app  = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cron = require('cron');
var logger = require('morgan');
var mongoose = require('mongoose');
var db = require('./config/db');
var indicatorsService = require('./services/indicators.service');
var notificationController = require('./controllers/api/notifications.controller');
var indicatorsSendHistoric = require('./services/indicatorsSendHistoric.service')

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override')); 
//Logs with morgan
app.use(logger('dev'));
//configs of bodyparser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

//Headers
app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Methods', ['GET','DELETE', 'OPTIONS', 'PUT', 'POST']);
    next();
});


//Conect to Dabase
mongoose.connect(db.url);

//Initialize and create the routes in /api
app.use('/api', require('./controllers/routes'));
var cronJob = cron.job("*/60 */60 * * * *", function(){
 indicatorsService.feedIndicators(function(response){
        if (response.result == 'error'){
            //console.log(response.result);
        }else {
            //console.log("response"+response);
        indicatorsSendHistoric.insertIndicator(response,function(resultado){
             
             if (resultado.result == "error" && resultado.titulo == null ){
                // console.log("resultado" + resultado.result + " titulo: "+resultado.titulo) ;
                console.log(resultado);
             }else if (resultado.titulo != null){
               console.log(resultado);
              indicatorsSendHistoric.middlewareNotification(resultado, function(response){
                  console.log(response);
              })
             }
         })
        }
     })
 });
   cronJob.start(); 
    

//Cron with the timers
// var cronJob = cron.job("*/60 */60 */60 * * *", function(){
//     // perform operation e.g. GET request http.get() etc.
//     console.log("Timer");
//       indicatorsService.feedIndicators(function(response){
//     if(response.tendencia == -1){
//         var image = "https://cdn3.iconfinder.com/data/icons/musthave/256/Stock%20Index%20Up.png";
//     }else {
//         var image = "https://cdn3.iconfinder.com/data/icons/musthave/256/Stock%20Index%20Down.png";
//     }
//   //notificationController.sendNotification(image, response);
// })
// });
// cronJob.start();

//Initialize the server
var server = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function(){
    var addr = server.address();
    console.log("API retool listening at", addr.address + ":" + addr.port);
});