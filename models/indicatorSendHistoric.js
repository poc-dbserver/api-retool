//indicator.js
/**
 *Indicators Schema 
 * 
 */
var mongoose = require('mongoose');
 
var indicatorSchema = mongoose.Schema({ 
    tipoGrupo: String,
    descricaoGrupo: String,
    tendencia: Number,
    indicador: String,
    tipoDadosComparacao: String,
    valor: Number,
    valorComparacao: Number,
    titulo: String,
    dia: Number,
    corpo: String,
    dataComparacao:  String,
    data: { type: Date }
 
});
indicatorSchema.index({dataComparacao:1, descricaoGrupo: 1}, { unique: true });

var Indicator = mongoose.model('indicatorSendHistoric', indicatorSchema);
module.exports= Indicator;