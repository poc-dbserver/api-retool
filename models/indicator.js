//indicator.js
/**
 *Indicators Schema 
 * 
 */
var mongoose = require('mongoose');
 
var indicatorSchema = mongoose.Schema({ 
    
    tipoGrupo: String,
    descricaoGrupo: String,
    tendencia: Number,
    indicador: String,
    dataComparacao:  String,
    tipoDadosComparacao: String,
    valor: Number,
    valorComparacao: Number,
    titulo: String,
    dia: Number,
    corpo: String,
    data: { type: Date }
 
});
 

var Indicator = mongoose.model('indicator', indicatorSchema);
module.exports= Indicator;